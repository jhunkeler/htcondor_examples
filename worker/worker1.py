#!/usr/bin/env python
''' All this does is:
    1. Open an input file
    2. Read a list of integers (x)
    3. Add to the integers: x += 1
    4. Write list of integers to a new file
'''

import os
import argparse

PARSER = argparse.ArgumentParser()
PARSER.add_argument('--output-dir', '-o', default=os.path.abspath(os.curdir))
PARSER.add_argument('INFILE', action='store', nargs='*', help='Input file')
ARGS = PARSER.parse_args()

def do_work(ifile):
    ofile = os.path.join(ARGS.output_dir, os.path.basename(ifile))
    in_data = []
    
    print('Loading {} ({} bytes)'.format(ifile, os.path.getsize(ifile)))
    
    with open(ifile, 'r') as fp:
        for line in fp:
            line = line.rstrip()
            in_data.append(line)
    
    with open(ofile, 'w+') as fp:
        for value in in_data:
            fp.writelines('{}{}'.format(int(value)+1, os.linesep))
            
    

if __name__ == '__main__':
    if not ARGS.INFILE:
        print("No input file(s) received!")
        exit(1)
        
    for infile in ARGS.INFILE:
        do_work(os.path.abspath(infile))
    
    exit(0)
